var path = require("path");
var express = require("express");
var app = express();
var http = require("http");
var SocketIo = require("socket.io");

var server = http.createServer(app);
var io = SocketIo.listen(server);

app.set("port",process.env.PORT || 4000);
app.use(express.static(path.join(__dirname,"public")));

server.listen(app.get("port"));
console.log("Server running on "+app.get("port"));

var line_history = [];

io.on('connection', function (socket) {
    for (var i in line_history) {
        socket.emit('draw_line', { line: line_history[i] } );
     }
     socket.on('draw_line', function (data) {
        line_history.push(data.line);
        io.emit('draw_line', { line: data.line });
     });
});